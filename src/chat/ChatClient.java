package chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javafx.scene.text.Text;

public class ChatClient extends UnicastRemoteObject implements ClientService {

	private static final long serialVersionUID = -2677503242277460448L;
	private String name;
	public ChatClient(String name) throws RemoteException {
		this.name = name;
	}
 
	public void send(String msg) throws RemoteException{

	}

	public String getName() throws RemoteException {
		return name;
	}
}