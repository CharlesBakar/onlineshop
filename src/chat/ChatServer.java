package chat;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

public class ChatServer extends UnicastRemoteObject implements ChatService {
	private List<String> userList = new ArrayList<String>();

	protected ChatServer() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -4770948517004604445L;

	@Override
	public synchronized boolean login(String name) throws RemoteException {
		if (userList.contains(name))
			return false;
		else
			return userList.add(name);
	}

	@Override
	public synchronized boolean logout(String name) throws RemoteException {
		return userList.remove(name);
	}

	@Override
	public synchronized void send(String msg) throws RemoteException {
		for (String name : userList) {
			ClientService client;
			try {
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				String myDate = sdf.format(new Date()).toString();
				client = (ClientService) Naming.lookup(name);
				client.send("[" + myDate + "]" + client.getName() + ":  " + msg);
			} catch (MalformedURLException | NotBoundException e) {
				logout(name);
				System.out.println("Delete non reachable user " + name);
			}
		}
	}

	@Override
	public synchronized List<String> getUserList() throws RemoteException {
		return userList;
	}

}
