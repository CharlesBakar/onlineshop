package controllers;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import chat.ChatClient;
import chat.ChatService;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import views.ViewChat;

public class ControllerChat {
	private ViewChat viewChat;
	private ChatClient chatClient;
	private ChatService chatServer;
	// private SimpleStringProperty username;
	private String username;
	private boolean isLogged = false;
	private boolean isRegistered = false;
	private Registry registry = null;
	private ObservableList<String> usersList = FXCollections.observableArrayList();
	private ObservableList<String> messages = FXCollections.observableArrayList();

	public ControllerChat(ViewChat viewChat) {
		this.viewChat = viewChat;
		viewChat.getUsers().setItems(usersList);
		viewChat.getMessages().setItems(messages);
		viewChat.setRegisterOnClickEvent(event -> {
			connect();
		});
		viewChat.setLoginOnClickEvent(event -> {
			if (isRegistered) {
				try {
					if (isLogged) {
						chatServer.logout(username);
						registry.unbind(username);
					}
					username = viewChat.getUsername();
					registry.rebind(username, chatClient);
					if (chatServer.login(username)) {
						System.out.println("Login successfull");
						isLogged = true;
						viewChat.setLoginDisable(true);
						viewChat.setLogoutDisable(false);
						viewChat.setMessageDisable(false);
						usersList.setAll(chatServer.getUserList());
					} else {
						System.out.println("Login Failed");
					}

				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e2) {
					e2.printStackTrace();
				}
			}

		});
		viewChat.setLogoutOnClickEvent(event -> {
			if(isRegistered){
				try {
					chatServer.logout(username);
					isLogged = false;
					viewChat.setLoginDisable(false);
					viewChat.setLogoutDisable(true);
					viewChat.setMessageDisable(true);
				} catch (RemoteException e1) {
					System.out.println("Logout Failed");
				}
			}
		});
		viewChat.setTextOnKeyReleased(event -> {
			if(isLogged){
				if (event.getCode() == KeyCode.ENTER) {
					try {
						chatServer.send(viewChat.getMessage());
						usersList.setAll(chatServer.getUserList());
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		});
	}

	private void connect() {
		try {
			registry = LocateRegistry.getRegistry("localhost", 1099);
			chatServer = (ChatService) registry.lookup("chatServer");
			chatClient = new ChatClient(username) {
				private static final long serialVersionUID = 3466617178514260166L;

				@Override
				public void send(String msg) throws RemoteException {
					messages.add(msg);
				}
				@Override
				public String getName() throws RemoteException {
					return username;
				}
			};
			// registered
			isRegistered = true;
			viewChat.setLoginDisable(false);
		} catch (RemoteException e) {
			System.out.println("Failed callling the stub");
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}
}