package controllers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

import helpers.TCPClient;
import implementation.Order;
import implementation.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import models.ModelShop;
import views.ViewCustomer;


public class ControllerCustomer {
	private Order order = new Order();
	TCPClient client = new TCPClient("localhost", 6666);
	ObservableList<fpt.com.Product> observableList = FXCollections.observableArrayList(order);

	public void link(ModelShop modelShop, ViewCustomer viewCustomer) {
		viewCustomer.getOrderTable().setItems(observableList);
		viewCustomer.getProductsList().setItems(modelShop);
		viewCustomer.addAddButtonHandler(e -> {
			fpt.com.Product p = viewCustomer.getSelectedProduct();
			System.out.println("Added " + p.getName());
			if(p!=null){
				observableList.add(p);
				order.add(p);
			}
		});
		viewCustomer.addBuyButtonHandler(e -> {
			client.setNewOrderAction((Order order) ->{
				System.out.println("Ack: Order clone received.");
			});
			viewCustomer.createLoginDialog(usernamePassword -> {
			   client.sendOrder(usernamePassword.getKey(), usernamePassword.getValue(), order);
			});
		});
//		viewCustomer.getOrdersTable().setItems(order);
	}
}