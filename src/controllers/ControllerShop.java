package controllers;

import java.io.EOFException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;

import com.thoughtworks.xstream.io.StreamException;

import fpt.com.SerializableStrategy;
import helpers.IDOverflow;
import implementation.ProductList;
import models.ModelShop;
import strategies.BinaryStrategy;
import strategies.JDBCStrategy;
import strategies.OpenJPAStrategy;
import strategies.XMLStrategy;
import strategies.XStreamStrategy;
import views.ViewShop;

public class ControllerShop {
	SerializableStrategy strategy;
	fpt.com.Product p;

	public void link(ModelShop modelShop, ViewShop viewShop) {
		viewShop.getProducts().setItems(modelShop);
		viewShop.addAddButtonHandler(e -> {
			p = viewShop.getInput();
			if (p != null) {
				/*try {
					long id = helpers.IDGenerator.getNewID(modelShop);
					p.setId(id);
					modelShop.add(p);
					System.out.println("ID = " + p.getId());
				} catch (IDOverflow eOverFlow) {
					System.out.println("zu hoch ; IDs sind benutzt");
				}*/
				modelShop.add(p);
			}
		});
		viewShop.addDeleteButtonHandler(f -> {
			//Get the index ID of the product in viewShop's productsList
			int index = viewShop.getSelectedProduct();
			if (index != -1)
				modelShop.remove(index);
		});
		viewShop.addSpeichernButtonHandler(event -> {
			strategy = viewShop.getSelectedStrategy();
			if (strategy != null) {

				try {
					if (strategy instanceof BinaryStrategy)
						strategy.open("products.ser");
					if (strategy instanceof XMLStrategy)
						strategy.open("products.xml");
					if (strategy instanceof XStreamStrategy)
						strategy.open("productsXstream.xml");
					if (strategy instanceof JDBCStrategy)
						strategy.open("DB");
					if (strategy instanceof OpenJPAStrategy)
						strategy.open("DB");
					for (fpt.com.Product savedProduct : modelShop)
						{
							System.out.println(savedProduct.getId());
							strategy.writeObject(savedProduct);
						}
					strategy.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		// Throw exception when reload class
		viewShop.addLadenButtonHandler(event -> {
			strategy = viewShop.getSelectedStrategy();
			if (strategy != null)
				try {
					if (strategy instanceof BinaryStrategy)
						strategy.open("products.ser");
					if (strategy instanceof XMLStrategy)
						strategy.open("products.xml");
					if (strategy instanceof XStreamStrategy)
						strategy.open("productsXstream.xml");
					if (strategy instanceof JDBCStrategy)
						strategy.open("DB");
					if (strategy instanceof OpenJPAStrategy)
						strategy.open("DB");
					while ((p = strategy.readObject()) != null) {
						if(!modelShop.add(p))
							System.out.println("Product ID "+p.getId() + " was not imported");
					}
					strategy.close();
				} catch (EOFException e) {
					System.out.println("Keine weiteren Objekte können geladen werden");
				} catch (StreamException e2) {
					System.out.println("Keine weiteren Objekte können geladen werden");
				}catch (AccessDeniedException e3){
					System.out.println("Die Datei konnte nicht geöffnet werden");
				}
				catch (IOException e4) {
					e4.printStackTrace();
				}
		});
	}

}
