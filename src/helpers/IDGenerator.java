package helpers;

import models.ModelShop;

public class IDGenerator {
	private static long IDCounter = 1;
	private final static long MAX_VALUE = 999999;
	public static long getNewID(ModelShop modelShop) throws IDOverflow{

		while(IDCounter <= MAX_VALUE && modelShop.findProductById(IDCounter)!=null) // prüfen ob ein Objekt mit derselben ID schin gibt
			IDCounter++;
		if(IDCounter>MAX_VALUE)
			throw new IDOverflow("IDs reached 999999");
		else
			return IDCounter++;

	}
	/*public static long getCurrentID(){
		return IDCounter;
	}*/
}