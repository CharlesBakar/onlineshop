package helpers;

public class IDOverflow extends Exception{

	/**
	 *
	 */
	private static final long serialVersionUID = -6738794628541131602L;

	public IDOverflow(String message)
	{
		super("IDs ist zu hoch");
	}
}