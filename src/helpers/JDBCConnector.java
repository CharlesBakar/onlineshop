
package helpers;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.CachedRowSet;

import fpt.com.Product;

public class JDBCConnector {
	public static void getDBDetails() {
		try {
			Class.forName("org.postgresql.Driver");
			try (Connection con = DriverManager.getConnection("jdbc:postgresql://java.is.uni-due.de/ws1011", "ws1011",
					"ftpw10")) {
				DatabaseMetaData databaseMetaData = con.getMetaData();
				String   catalog          = null;
				String   schemaPattern    = null;
				String   tableNamePattern = null;
				String[] types            = null;

				ResultSet result = databaseMetaData.getTables(
				    catalog, schemaPattern, tableNamePattern, new String[]{"TABLE"} );

				System.out.println("URL: " + databaseMetaData.getURL());
				System.out.println("Username: " + databaseMetaData.getUserName());
				while(result.next()) {
//				    String tableName = result.getString(3);
//				    System.out.println(tableName + "Catalog " + result.getString(1) + "Scheme " + result.getString(2));
				System.out.println(result.getString(3));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		}
	}

	public static long insert(String name, double price, int quantity){
		try {
			Class.forName("org.postgresql.Driver");
			try (Connection con = DriverManager.getConnection("jdbc:postgresql://java.is.uni-due.de/ws1011", "ws1011",
					"ftpw10")) {
				try(PreparedStatement pstmt = con.prepareStatement("INSERT INTO products(name,price,quantity) VALUES (?,?,?)",Statement.RETURN_GENERATED_KEYS)){
					pstmt.setString(1, name);
					pstmt.setDouble(2, price);
					pstmt.setInt(3, quantity);
					if(pstmt.executeUpdate()!=0){
						ResultSet generatedKeys = pstmt.getGeneratedKeys();
						if(generatedKeys.next())
							return generatedKeys.getLong(1);
					}
				}catch(SQLException e){
					e.printStackTrace();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		}
		return -1;
	}

	public static void insert(Product p){
		p.setId(insert(p.getName(), p.getPrice(), p.getQuantity()));
	}
	public static Product read (long productId){
		Product p = null;
		try {
			Class.forName("org.postgresql.Driver");
			try (Connection con = DriverManager.getConnection("jdbc:postgresql://java.is.uni-due.de/ws1011", "ws1011",
					"ftpw10");
					PreparedStatement pstmt = con.prepareStatement("SELECT id,name,price,quantity FROM products WHERE id = ?")) {
						pstmt.setLong(1, productId);
						ResultSet rs = pstmt.executeQuery();
						while(rs.next()){
							p = new implementation.Product();
							p.setId(rs.getLong("id"));
							p.setName(rs.getString("name"));
							p.setPrice(rs.getDouble("price"));
							p.setQuantity(rs.getInt("quantity"));
						}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		}
		return p;
	}
}