package helpers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

import implementation.Main;
import implementation.Order;
import implementation.Product;

public class TCPClient {
	private Socket warehouseSocket = null;
	private boolean connectionEstablished = false;
	private String hostname;
	private int portNumber;
	private IncomingThread incomingThread = null;
	private OutgoingThread outgoingThread = null;
	private callbackAction newOrderAction;

	public TCPClient(String hostname, int portNumber) {
		this.hostname = hostname;
		this.portNumber = portNumber;
	}

	public void sendOrder(String username, String password, Order order) {
		try {
			if (!getStatus()) {
				this.connect();
			}
			outgoingThread.sendOrder(username, password, order);

		} catch (IOException e) {

		}
	}

	public void connect() throws IOException {
		if (warehouseSocket == null) {
			warehouseSocket = new Socket(hostname, portNumber);
			incomingThread = new IncomingThread(warehouseSocket.getInputStream(),this);
			outgoingThread = new OutgoingThread(warehouseSocket.getOutputStream(),this);
			Main.mainExecutorService.execute(incomingThread);
			Main.mainExecutorService.execute(outgoingThread);
			setStatus(true);
		}
	}

	public synchronized void close() {
		if(getStatus()){
			try {
				warehouseSocket.close();
				warehouseSocket = null;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			setStatus(false);
		}
	}
	public synchronized boolean getStatus(){
		return connectionEstablished;
	}
	public synchronized void setStatus(boolean status){
		connectionEstablished = status;
	}
	public void addOrder(Order order){
		newOrderAction.action(order);
	}
	public void setNewOrderAction(callbackAction action){
		newOrderAction = action;
	}
}

class IncomingThread implements Runnable {
	private InputStream inputStream;
	private DataInputStream inputDataStream;
	private TCPClient tcpClient;

	public IncomingThread(InputStream in,TCPClient client) {

		tcpClient = client;
		inputStream = in;
		inputDataStream = new DataInputStream(in);
	}

	@Override
	public void run() {
		//System.out.println("Incomingthread "+Thread.currentThread().getName());
		while (tcpClient.getStatus() && !Thread.currentThread().isInterrupted()) {
			try{
				String command = inputDataStream.readUTF();
				Scanner sc = new Scanner(command).useDelimiter(":");

				// Erstes Kommando filtern
				String keyword = sc.next();

				if (keyword.equals("clone")) {
					try {
						ObjectInputStream inputObjectStream = new ObjectInputStream(inputStream);
						Order order;
						try {
							order = (Order) inputObjectStream.readObject();
							tcpClient.addOrder(order);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}else if (keyword.equals("denied")){
					System.out.println("Worng username or password!");
				}
			}catch(IOException e){
				System.out.println("InputThread error");
				tcpClient.close();
			}

		}
		tcpClient.close();
	}

}

class OutgoingThread implements Runnable {
	private OutputStream outputStream;
	private DataOutputStream outputDataStream;
	private TCPClient tcpClient;

	public OutgoingThread(OutputStream out,TCPClient client) {

		tcpClient = client;
		outputStream = out;
		outputDataStream = new DataOutputStream(out);
	}

	@Override
	public void run() {
		//System.out.println("Outgoingthreas "+Thread.currentThread().getName());
		while (tcpClient.getStatus() && !Thread.currentThread().isInterrupted()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				tcpClient.close();
			}
		}
		tcpClient.close();
	}

	public void sendOrder(String username, String password, Order order) {
		try {
			outputDataStream.writeUTF("order:" + username + ":" + password);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(order);
			outputStream.flush();
		} catch (IOException e) {
			System.out.println("Could not send the order");
			tcpClient.close();
		}
	}
}