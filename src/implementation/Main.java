package implementation;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import controllers.ControllerChat;
import controllers.ControllerCustomer;
import controllers.ControllerShop;
import helpers.JDBCConnector;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.ModelShop;
import views.ViewChat;
import views.ViewCustomer;
import views.ViewShop;

public class Main extends Application{

	public static final ScheduledExecutorService mainExecutorService = Executors.newScheduledThreadPool(5);

	public static void main(String[] args) {
		//JDBCConnector.getDBDetails();
		//System.out.println("ID of the newly inserted product: "+JDBCConnector.insert("DirectInsert",100.29,2));
	//	System.out.println(JDBCConnector.read(14546651).getName());
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		ModelShop modelShop = new ModelShop();
		ViewShop viewShop = new ViewShop();
		ControllerShop controllerShop = new ControllerShop();
		controllerShop.link(modelShop, viewShop);
		stage.setScene(new Scene(viewShop));
		stage.setTitle("View Shop");

		ViewCustomer viewCustomer = new ViewCustomer();
		ControllerCustomer controllerCustomer = new ControllerCustomer();
		Stage customerStage = new Stage();
		customerStage.setScene(new Scene(viewCustomer));
		controllerCustomer.link(modelShop, viewCustomer);
		customerStage.setTitle("View Customer");
		customerStage.show();
		stage.show();

		ViewChat viewChat = new ViewChat();
		ControllerChat controllerChat = new ControllerChat(viewChat);
		Stage chatStage = new Stage();
		chatStage.setScene(new Scene(viewChat));
		chatStage.setTitle("View Chat");
		chatStage.show();
		stage.setOnCloseRequest(e -> {
			mainExecutorService.shutdownNow();
			customerStage.close();
			chatStage.close();
		});

	}
}
