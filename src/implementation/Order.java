package implementation;

import java.util.ArrayList;

import fpt.com.Product;

public class Order extends ArrayList<Product> implements fpt.com.Order {

	/**
	 *
	 */
	private static final long serialVersionUID = -3362134429201775477L;
	Product product;
	double summe;

// Should not be implemented !, it will cause a recursive call as this function is already implemented in ArrayList
//	@Override
//	public boolean add(Product e) {
//
//		if (super.add(e)) {
//			return true;
//		} else
//			return false;
//
//	}

	@Override
	public boolean delete(Product p) {
		if (this.remove(p)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int size() {
		return size();
	}

	@Override
	public Product findProductById(long id) {
		for (Product product : this) {
			if (product.getId() == id) {
				return product;
			}
		}
		return null;
	}

	@Override
	public Product findProductByName(String name) {
		for (Product product : this) {
			if (product.getName().equals(name)) {
				return product;
			}
		}
		return null;
	}

	@Override
	public double getSum() {
		double sum = 0;
		for (Product product : this) {
			sum += product.getPrice() * product.getQuantity();
		}
		return sum;
	}

	@Override
	public int getQuantity() {
		int quantity = 0;
		for (Product product : this) {
			quantity += product.getQuantity();
		}
		return quantity;
	}

}
