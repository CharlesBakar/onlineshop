package implementation;


import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.openjpa.persistence.Persistent;
import org.apache.openjpa.persistence.jdbc.Strategy;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAliasType;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Entity()
@Table(name="products")
@XStreamAliasType("ware")
@XStreamAlias("ware")
public class Product implements fpt.com.Product, Externalizable {

	//@XStreamConverter(IDPropertyConverter.class)
	@XStreamAlias("id")
	@XStreamAsAttribute
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "products_SEQ")
	private long id;

	//@XStreamConverter(NamePropertyConverter.class)
	@XStreamAlias("name")
	@Persistent
	@Strategy("fpt.com.db.StringPropertyValueHandler")
	private SimpleStringProperty name = new SimpleStringProperty();

	//@XStreamConverter(PriceProperteryConverter.class)
	@XStreamAlias("preis")
	@Persistent
	@Strategy("fpt.com.db.DoublePropertyValueHandler")
	private SimpleDoubleProperty price;// = new SimpleDoubleProperty();

	//@XStreamConverter(QuantityPropertyConverter.class)
	@XStreamAlias("anzahl")
	@Persistent
	@Strategy("fpt.com.db.IntegerPropertyValueHandler")
	private SimpleIntegerProperty quantity;// = new SimpleIntegerProperty();

	public Product() {
		this.name = new SimpleStringProperty();
		this.price = new SimpleDoubleProperty();
		this.quantity = new SimpleIntegerProperty();

	}

	public Product(long id, String name, double price, int quantity) {
		  this.id = id;
		  this.name = new SimpleStringProperty(name);
	      this.price = new SimpleDoubleProperty(price);
	      this.quantity = new SimpleIntegerProperty(quantity);
	}
	public Product( String name, double price, int quantity) {
		  this.name = new SimpleStringProperty(name);
	      this.price = new SimpleDoubleProperty(price);
	      this.quantity = new SimpleIntegerProperty(quantity);
	}
	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public double getPrice() {
		return price.get();
	}

	@Override
	public void setPrice(double price) {
		this.price.set(price);
	}

	@Override
	public int getQuantity() {
     	return quantity.get();
	}

	@Override
	public void setQuantity(int quantity) {
		this.quantity.set(quantity);
	}

	@Override
	public String getName() {
		return name.get();
	}

	@Override
	public void setName(String name) {
     this.name.set(name);
	}

	@Override
	public StringProperty nameProperty() {
		return name;
	}

	@Override
	public DoubleProperty priceProperty() {
		return price;
	}

	@Override
	public IntegerProperty quantityProperty() {
		return quantity;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(id);
		out.writeObject(getName());
		out.writeDouble(getPrice());
		out.writeInt(getQuantity());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

		setId(in.readLong());
		setName((String)in.readObject());
		setPrice(in.readDouble());
		setQuantity(in.readInt());
	}

}
