package implementation;

import java.util.ArrayList;
import fpt.com.Product;

public class ProductList  extends ArrayList<Product> implements fpt.com.ProductList {

	/**
	 *
	 */
	private static final long serialVersionUID = -8824337604760145830L;
	Product product;

//  Should not be implemented !, it will cause a recursive call as this function is already implemented in ArrayList
//	@Override
//	public boolean add(Product e) {
//
//		if (this.add(e)) {
//			return true;
//		}else
//			return false;
//	}

	@Override
	public boolean delete(Product product) {
	   if (this.remove(product)) {
		   return true;
	   }else {
		   return false;
	   }
	}

	@Override
	public Product findProductById(long id) {
		for (Product product : this) {
		 if( product.getId() == id) {
			 return product;
		 }
	   }
	   return null;
	}

	@Override
	public Product findProductByName(String name) {
		for (Product product : this) {
			if (product.getName().equals(name)) {
				return product;
			}
		}
		return null;
	}

}
