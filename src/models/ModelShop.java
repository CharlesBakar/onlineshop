package models;
import fpt.com.Product;
import implementation.ProductList;
import javafx.collections.ModifiableObservableListBase;

public class ModelShop extends ModifiableObservableListBase<fpt.com.Product> implements fpt.com.ProductList{

	private implementation.ProductList plist = new ProductList();

	@Override
	public boolean delete(Product product) {
		return plist.delete(product);
	}

	@Override
	public Product findProductById(long id) {
		return plist.findProductById(id);
	}

	@Override
	public Product findProductByName(String name) {
		return plist.findProductByName(name);
	}

	@Override
	public boolean add(Product element) {
		/*return super.add(element);
		 Disabled because the previous ID strategy class is not used anymore*/
		try{
			if(element.getName()!=null){
				if(element.getId() == 0L || findProductById(element.getId())==null)
					{
						return super.add(element);
					}
				else{
					System.out.println("Duplicated ID "+ element.getId());
					return false;
					}

			}else
				return false;
		}catch(NullPointerException e){
			System.out.println("LOADED DATA ! KEINEN NAME ! FEHLER AVOIDED");
			return false;
		}
	}

	@Override
	protected void doAdd(int index, Product element) {
		plist.add(index, element);
	}

	@Override
	protected Product doRemove(int index) {
		return plist.remove(index);
	}

	@Override
	protected Product doSet(int index, Product element) {
		return plist.set(index, element);
	}

	@Override
	public Product get(int index) {
		return plist.get(index);
	}

	@Override
	public int size() {
		return plist.size();
	}
}