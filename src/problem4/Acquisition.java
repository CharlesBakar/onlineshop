package problem4;

import java.util.Random;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class Acquisition implements Runnable{

	Random randomGenerator = new Random();
	Cashpoint[] kassen;
	boolean keepAkquisition = true;
	Lock lock;
	ThreadPoolExecutor executor;
	Long currentTime;

	public Acquisition(Cashpoint[] kassen,Lock lock,ThreadPoolExecutor executor) {
		this.kassen = kassen;
		this.lock = lock;
		this.executor = executor;
	}

	@Override
	public void run() {
		System.out.println("Started Acquisition Process");
		while(keepAkquisition){
			try {
				TimeUnit.SECONDS.sleep(randomGenerator.nextInt(3));
			} catch (InterruptedException e) {
				System.err.println("Sleep faild because of InterruptedException");
			}
			lock.lock();
			System.out.println("Lock Acquisition");
				int cashpointIndex = 0, cashpointWaitingCustomer = kassen[0].getWaitingCustomers();
				 //if 8 customers in one waitingList are founded
				for(int i=1;i<kassen.length;i++){
					if(kassen[i].isOpened()){
							if(kassen[i].getWaitingCustomers() < cashpointWaitingCustomer){
								cashpointIndex = i;
								cashpointWaitingCustomer = kassen[i].getWaitingCustomers();
							}
					}
				}
				//Add Customer
				Customer customer = new Customer(randomGenerator.nextInt(10000));
				kassen[cashpointIndex].addCustomer(customer);
				Main.printDebug("NeuerKunde \t" + " Cashpoint " + (cashpointIndex+1) + "\t Left Customers " + kassen[cashpointIndex].getWaitingCustomers() + " $ " + customer.getPayment());
				//ِAdded Customer
				if(kassen[cashpointIndex].getWaitingCustomers()==6){ // Should open a new Kasse
					openCashpoint();
					Main.printDebug("Submitted \t" + " Cashpoint " + (cashpointIndex+1));
				}
				if(kassen[cashpointIndex].getWaitingCustomers()==8){ // Should open a new Kasse
					keepAkquisition = false;
					Main.printDebug("Acqu Ended !");
					//continue;
				}
				System.out.println("Unlock Acquisition");
			lock.unlock();
		}
		executor.shutdown();
	}
	private void openCashpoint(){
		int x = 0,lowerClosedIndex = -1,openedCashpoints = 0;
		boolean openNewCashpoint = false;
			for(x=0;x<kassen.length;x++){
				if(!kassen[x].isOpened() && lowerClosedIndex  == -1)
					lowerClosedIndex = x;
				if(kassen[x].isOpened()){
					openedCashpoints++;
					// for debug purposes
					if(kassen[x].getWaitingCustomers() == 6 && !openNewCashpoint)
						openNewCashpoint = true;
				}
			}
			if(openNewCashpoint && openedCashpoints < 6){ //open new cashpoint
				executor.submit(kassen[lowerClosedIndex]);
			}
	}
}