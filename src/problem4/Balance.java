package problem4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class cashpointBalance implements Comparator<cashpointBalance>{
	private int ID;
	private double umsatz;
	public cashpointBalance(int id) {
		ID = id;
		umsatz = 0.0;
	}
	public cashpointBalance() {

	}
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}
	public double getUmsatz(){
		return umsatz;
	}
	public void increaseUmsatz(double payment){
		umsatz+=payment;
	}
	@Override
	public int compare(cashpointBalance o1, cashpointBalance o2) {
		//decensding order
		return (int)-(o1.getUmsatz() - o2.getUmsatz());
	}
}
public class Balance {
    List<cashpointBalance>  balanceList = new ArrayList<cashpointBalance>();

    public Balance(){
    	for(int i=1;i<=6;i++){
        	balanceList.add(new cashpointBalance(i));
        }
    }
    public synchronized void increaseSales(int id, double payment){
    	balanceList.forEach(element -> {
    		if(element.getID()==id)
    			element.increaseUmsatz(payment);
    	});
    	Collections.sort(balanceList,new cashpointBalance());
    	//print
    	String output = new String("Kassen <ID,Umsatz>");
    	for(cashpointBalance b : balanceList){
    		output = output + "<"+ b.getID() + "/"+  String.format("%1$,.2f", b.getUmsatz()) + ">\t";
    	}
    	System.out.println(output);
    }
    public synchronized void sortSales(){

    }
    public double getSales(int id){
    	for(cashpointBalance b : balanceList){
    		if(b.getID()==id)
    			return b.getUmsatz();
    	}
    	System.out.println("NOT FOUND !");
    	return -1;
    }
}
