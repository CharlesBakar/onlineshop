package problem4;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import problem4.Customer;

public class Cashpoint implements Runnable{

	boolean opened;
	ArrayList<Customer> waitingList;
	int ID;
	Lock lock;
	Random randomGenerator = new Random();
	Balance balance;
	Long currentTime;
	public Cashpoint(){

	}
	public Cashpoint(int ID,Balance balance, Lock lock){
		waitingList = new ArrayList<Customer>();
		this.ID = ID;
		opened = false;
		this.lock = lock;
		this.balance = balance;
	}

	@Override
	public void run() {
		opened = true;
		Main.printDebug("Opened \t" + " Cashpoint " + ID );
		try {
			TimeUnit.SECONDS.sleep(6);
		} catch (InterruptedException e) {
			System.out.println("Opening Cashpoint was interrupted");
		}
		Main.printDebug("SProcessing \t" + " Cashpoint " + ID );
		while(!waitingList.isEmpty()){
			try {
				int waitTime = randomGenerator.nextInt(5)+6;
				TimeUnit.SECONDS.sleep(waitTime); //ofcourse the existing customer will stay but we may have new one
			} catch (InterruptedException e) {
				System.err.println("Opening Cashpoint was interrupted");
			}
			lock.lock();// so acquistion gets accurate data of whom has least customers
			System.out.println("Lock Cashpoint für "+this.ID);
			double payment = waitingList.remove(0).getPayment();
			//System.out.println("Abarbeitung von Kunden an der Kasse ID "+ID + " Wert " + payment + " left "+waitingList.size());
			Main.printDebug("Abarbeitung \t" + " Cashpoint " + ID + "\t Left Customers " + waitingList.size() + " € " + payment);
			balance.increaseSales(ID, payment);
			lock.unlock();
			System.out.println("Unlock Cashpoint für " +this.ID);
		}
			Main.printDebug("Closed \t" + " Cashpoint " + ID + "\t Umsatz " + balance.getSales(ID));
	}

	public boolean isOpened(){
		return opened;
	}
	public int getWaitingCustomers(){
		return waitingList.size();
	}
	public void addCustomer(Customer c){
		waitingList.add(c);
	}
	public void removeCustomer(){
		waitingList.remove(0);
	}
	public int getID(){
		return ID;
	}
}