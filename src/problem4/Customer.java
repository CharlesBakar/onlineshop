package problem4;

public class Customer {
	private double payment;

	public Customer(double payment){
		this.payment = payment;
	}
	public double getPayment() {
		return payment;
	}

	/*public void setPayment(double payment) {
		this.payment = payment;
	}*/
}