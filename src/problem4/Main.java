package problem4;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
	public static Long debugClock = System.nanoTime();

	public static void main(String[] args) {

		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(7);
		Cashpoint[] kassen = new Cashpoint[6];
		Balance balance = new Balance();

		Lock customersManiplulationsLock = new ReentrantLock();

		for(int i=0;i<6;i++)
			kassen[i] = new Cashpoint(i+1,balance,customersManiplulationsLock);

		Acquisition customersGenerator = new Acquisition(kassen,customersManiplulationsLock,executor);

		executor.submit(customersGenerator);
		executor.submit(kassen[0]);
		/*
		Balance b = new Balance();
		b.increaseSales(1, 100);
		b.increaseSales(2, 200);
		b.increaseSales(3, 300);
		b.increaseSales(1, 500);
		*/
	}
	public static void printDebug(String str){
		Long currentTime = (System.nanoTime() - Main.debugClock)/(1000000000);
		System.out.format("%3d %s\n", currentTime, str);
	}
}