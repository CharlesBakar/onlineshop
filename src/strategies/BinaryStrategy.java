package strategies;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import fpt.com.Product;
import fpt.com.SerializableStrategy;

// binäre Serialisierung


public class BinaryStrategy implements SerializableStrategy{
	private InputStream inputStream = null;
	private OutputStream outputStream = null;
	private ObjectOutputStream outputObject = null;
	private ObjectInputStream inputObject = null;

	@Override
	public Product readObject() throws IOException {
		Product p = null;
		if(inputObject!=null){
			try {
				p = (Product) inputObject.readObject();
				System.out.println(p.getName());
			}catch(ClassNotFoundException e) {
				System.out.println("Binär : Loading Class ; types did not match");
			}
		}
		return p;
	}

	@Override
	public void writeObject(Product obj) throws IOException {
		if(outputObject!=null){
			outputObject.writeObject(obj);
			outputObject.flush();
		}
	}
	@Override
	public void close() throws IOException {
		if (inputObject != null) inputObject.close();
		if (outputObject != null) outputObject.close();
		if (inputStream != null) inputStream.close();
		if (outputStream != null) outputStream.close();
	}
	@Override
	public void open(InputStream input, OutputStream output) throws IOException {
		if(input != null){
			inputStream = input;
			inputObject = new ObjectInputStream(input);
		}
		outputStream = output;
		outputObject = new ObjectOutputStream(output);
	}
}