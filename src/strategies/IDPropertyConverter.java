package strategies;

import java.util.Locale;

import com.thoughtworks.xstream.converters.SingleValueConverter;

public class IDPropertyConverter implements SingleValueConverter{

	@Override
	public boolean canConvert(Class type) {
		return type.equals(Long.class);
	}

	@Override
	public String toString(Object obj) {
		//sechsstellig mit Punkt sttat Komma als trenner
		return String.format(Locale.US,"%06d",((Long) obj));
	}

	@Override
	public Object fromString(String str) {
		Long ID = Long.parseLong(str);
		return ID;
	}

}
