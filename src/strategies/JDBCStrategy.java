package strategies;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fpt.com.Product;
import fpt.com.db.AbstractDatabaseStrategy;
import implementation.ProductList;

public class JDBCStrategy extends AbstractDatabaseStrategy{
	ProductList pl = null;
	Connection con = null;
	final int maxRows = 10;
	@Override
	public Product readObject() throws IOException {
		if(pl!=null && !pl.isEmpty())
			return pl.remove(0);
		else{
			pl = null;
			return null;
		}
	}

	@Override
	public void writeObject(Product obj) throws IOException {
		if(con != null && obj.getId()==0L){
			try {
				Class.forName("org.postgresql.Driver");
					try(PreparedStatement pstmt = con.prepareStatement("INSERT INTO products(name,price,quantity) VALUES (?,?,?)",Statement.RETURN_GENERATED_KEYS)){
						pstmt.setString(1, obj.getName());
						pstmt.setDouble(2, obj.getPrice());
						pstmt.setInt(3, obj.getQuantity());
						if(pstmt.executeUpdate()!=0){
							ResultSet generatedKeys = pstmt.getGeneratedKeys();
							if(generatedKeys.next())
								obj.setId(generatedKeys.getLong(1));
						}
					}catch(SQLException e){
						e.printStackTrace();
					}
			} catch (ClassNotFoundException e2) {
				e2.printStackTrace();
			}
		}
	}

	@Override
	public void close() throws IOException {
		if(con!=null)
			try {
				con.close();
				con = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void open() throws IOException {
			//füllen und von readObject each element pop and return (200 elements)
			try {
				Class.forName("org.postgresql.Driver");
				con = DriverManager.getConnection("jdbc:postgresql://java.is.uni-due.de/ws1011", "ws1011",
						"ftpw10");
				if(pl==null){
					Statement stmt = con.createStatement();
					stmt.setMaxRows(maxRows);
					ResultSet rs = stmt.executeQuery("SELECT id,name,price,quantity FROM products ORDER BY id DESC");
					Product p ;
					pl = new implementation.ProductList();
						while(rs.next()){
							p = new implementation.Product();
							System.out.println(rs.getLong("id"));
							p.setId(rs.getLong("id"));
							p.setName(rs.getString("name"));
							p.setPrice(rs.getDouble("price"));
							p.setQuantity(rs.getInt("quantity"));
							pl.add(p);
						}
				}
			}catch(SQLException e){
				e.printStackTrace();
			}catch(ClassNotFoundException e2){
				e2.printStackTrace();
			}
	}

}