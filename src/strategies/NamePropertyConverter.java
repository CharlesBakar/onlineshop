package strategies;

import com.thoughtworks.xstream.converters.SingleValueConverter;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NamePropertyConverter implements SingleValueConverter{

	@Override
	public boolean canConvert(Class type) {
		return (type.equals(StringProperty.class) || type.equals(SimpleStringProperty.class));
	}

	@Override
	public String toString(Object obj) {
		return ((SimpleStringProperty) obj).get();
	}

	@Override
	public Object fromString(String str) {
		SimpleStringProperty name = new SimpleStringProperty(str);
		return name;
	}

}
