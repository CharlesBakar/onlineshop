package strategies;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.openjpa.persistence.InvalidStateException;
import org.apache.openjpa.persistence.OpenJPAPersistence;

import fpt.com.Product;
import fpt.com.db.AbstractDatabaseStrategy;
import implementation.ProductList;

public class OpenJPAStrategy extends AbstractDatabaseStrategy {
	EntityManagerFactory entityManagerFactory = null;
	EntityManager entityManager = null;
	ProductList pl = null;
	final int maxRows = 10;

	@Override
	public Product readObject() throws IOException {
		if(pl!=null && !pl.isEmpty())
			return pl.remove(0);
		else{
			pl = null;
			return null;
		}
	}

	@Override
	public void writeObject(Product obj) throws IOException {
		EntityTransaction transcation = entityManager.getTransaction();
		if(obj.getId()==0L){
			try{
				transcation.begin();
				entityManager.persist(obj);
				transcation.commit();
			}catch (InvalidStateException e){
				System.out.println("PK !");
			}
		}
	}

	@Override
	public void close() throws IOException {
		if (entityManager != null)
			entityManager.close();
		if (entityManagerFactory != null)
			entityManagerFactory.close();
	}

	@Override
	public void open() throws IOException {
		entityManagerFactory = Persistence.createEntityManagerFactory("openjpa",System.getProperties());
		//entityManagerFactory = getWithoutConfig();
		System.out.println(System.getProperties());
		entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction transcation = entityManager.getTransaction();

		transcation.begin();
		Query q = entityManager.createQuery("SELECT p FROM Product p ORDER BY p.id DESC");
		q.setMaxResults(maxRows);
		transcation.commit();
		//q.setFirstResult(-1);
		pl = new ProductList();
		for (Object o : q.getResultList()) {
			pl.add((Product) o);
		}
	}
	public static EntityManagerFactory getWithoutConfig() {

		HashMap<String, String> map = new HashMap<String, String>();
         map.put("openjpa.ConnectionURL", "jdbc:postgresql://java.is.uni-due.de/ws1011");
         map.put("openjpa.ConnectionDriverName", " org.postgresql.Driver");
         map.put("openjpa.ConnectionUserName", "ws1011");
         map.put("openjpa.ConnectionPassword", "ftpw10");
         map.put("openjpa.RuntimeUnenhancedClasses", "supported");
         map.put("openjpa.jdbc.SynchronizeMappings", "false");
         map.put("openjpa.MetaDataFactory", "jpa(Types=" + implementation.Product.class.getName() + ")");

         /*
         List<Class<?>> types = new ArrayList<Class<?>>();
         types.add(implementation.Product.class);
		 if(!types.isEmpty()) {
        	 StringBuffer buf = new StringBuffer();
        	 for(Class<?> c: types) {
        		 if(buf.length() > 0)
        			 buf.append(";");
        		 buf.append(c.getName());
        	 }
        	 map.put("openjpa.MetaDataFactory", "jpa(Types=" + buf.toString() + ")");
         }
         */
         return OpenJPAPersistence.getEntityManagerFactory(map);

	}
}
