package strategies;

import java.util.Locale;

import com.thoughtworks.xstream.converters.SingleValueConverter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PriceProperteryConverter implements SingleValueConverter{

	@Override
	public boolean canConvert(Class type) {
		return true;//(type.equals(DoubleProperty.class) || type.equals(SimpleDoubleProperty.class));
	}

	@Override
	public String toString(Object obj) {
		// set the resolution of price to 2 decimals places
		return String.format(Locale.US,"%.2f",((SimpleDoubleProperty) obj).get());
	}

	@Override
	public Object fromString(String str) {
		DoubleProperty price = new SimpleDoubleProperty(Double.parseDouble(str));
		return price;
	}

}
