package strategies;

import com.thoughtworks.xstream.converters.SingleValueConverter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class QuantityPropertyConverter implements SingleValueConverter{

	@Override
	public boolean canConvert(Class type) {
		return true;//(type.equals(IntegerProperty.class) || type.equals(SimpleIntegerProperty.class));
	}

	@Override
	public String toString(Object obj) {
		return String.valueOf(((SimpleIntegerProperty) obj).get());
	}

	@Override
	public Object fromString(String str) {
		IntegerProperty quantity = new SimpleIntegerProperty(Integer.parseInt(str));
		return quantity;
	}

}
