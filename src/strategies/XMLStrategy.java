package strategies;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import fpt.com.Product;
import fpt.com.SerializableStrategy;



public class XMLStrategy implements SerializableStrategy, AutoCloseable{

	private InputStream fis = null;
	private OutputStream fos = null;
	private XMLEncoder encoder = null;
	private XMLDecoder decoder = null;

	@Override
	public Product readObject() throws IOException  {
		Product product = null;
		if(decoder!= null){
			try{
				product = (Product) decoder.readObject();
			}catch (ArrayIndexOutOfBoundsException e){
				System.out.println("Keine weiteren Objekte können geladen werden");
			}
		}
		return product;
	}

	@Override
	public void writeObject(Product obj) throws IOException {
		if (encoder != null) {
			encoder.writeObject(obj);
			encoder.flush();
		}
	}

	@Override
	public void close() throws IOException {
		if (decoder != null) {
			decoder.close();
			decoder = null;
		}
		if (encoder != null) {
			encoder.close();
			encoder = null;
		}
		if (fis != null) {
			fis.close();
			fis = null;
		}
		if (fos != null) {
			fos.close();
			fos = null;
		}
	}
	@Override
	public void open(InputStream input, OutputStream output) throws IOException {
		if(input!= null){
			fis = input;
			decoder = new XMLDecoder(input);
		}
		fos = output;
		encoder = new XMLEncoder(output);
	}
}