package strategies;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;

import fpt.com.Product;
import fpt.com.SerializableStrategy;


public class XStreamStrategy implements SerializableStrategy{

	private XStream xstream;
	private ObjectOutputStream oos;
	private ObjectInputStream iis;

	@Override
	public Product readObject() throws IOException{
		Product p=null;
		try {
			p = (Product)(iis.readObject());

		}catch(NullPointerException e) {
			System.out.println("Keine weiteren Objekte können geladen werden");
		} catch (ClassNotFoundException e) {
			System.out.println("Binär : Loading Class ; types did not match");
		}
		return p;

	}

	@Override
	public void writeObject(fpt.com.Product obj) throws IOException {
		oos.writeObject(obj);
	}
	@Override
	public void close() throws IOException {
		if(oos!=null){
			oos.close();
			oos=null;
		}
		if(iis!=null){
			iis.close();
			iis=null;
		}
	}

	@Override
	public void open(InputStream input, OutputStream output) throws IOException {
		//Die Annotation innerhalb der Klasse Product soll beim XStream bearbeitet werden
		xstream = createXStream(implementation.Product.class);
		oos= xstream.createObjectOutputStream(output, "waren");
		if(input != null){
			try{ // Wenn zunächst die Datei eröffnen, eine Exception geworfen wird, weil keine valid InputStream gibt
				iis = xstream.createObjectInputStream(input);
			}catch(StreamException e2){
				System.out.println("Konnte die Datei nicht laden");
			}
		}
	}

}