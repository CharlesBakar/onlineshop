package views;

import fpt.com.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;

public class ViewChat extends BorderPane{
	private VBox rightBar = new VBox(5);
	private ListView<String> usersList = new ListView<String>();
	private ListView<String> messagesList = new ListView<String>();
	private TextField message = new TextField();
	private TextField username = new TextField("Username");
	private Button login = new Button("Login");
	private Button logout = new Button("Logout");
	private Button register = new Button("Register");

	public ViewChat(){
		login.setDisable(true);
		logout.setDisable(true);
		message.setDisable(true);
		rightBar.getChildren().addAll(usersList,username,login,logout,register);
		rightBar.setPrefHeight(USE_COMPUTED_SIZE);
		setCenter(messagesList);
		setBottom(message);
		setRight(rightBar);
		setMinWidth(400);
		setMinHeight(500);
		
		
		Callback<ListView<String>, ListCell<String>> cb = new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> param) {
				return new MyCustomCell();
			}
		};
		usersList.setCellFactory(cb);
		
		
		usersList.setCellFactory(c -> {

			ListCell<String> cell = new ListCell<String>() {
				@Override
				protected void updateItem(String myObject, boolean b) {
					super.updateItem(myObject, myObject == null || b);
					if (myObject != null) {
						setText("'" + myObject + "'" + " " + "has joined the chatroom");
					} else {
						// wichtig da sonst der text stehen bleibt!
						setText("");
					}
				}

			};
			return cell;

		});
	}
	public void setTextOnKeyReleased(EventHandler<KeyEvent> eventHandler){
		message.setOnKeyReleased(eventHandler);
	}
	public void setLoginOnClickEvent(EventHandler<ActionEvent> eventHandler){
		login.addEventHandler(ActionEvent.ACTION, eventHandler);
	}
	public void setLogoutOnClickEvent(EventHandler<ActionEvent> eventHandler){
		logout.addEventHandler(ActionEvent.ACTION, eventHandler);
	}
	public void setRegisterOnClickEvent(EventHandler<ActionEvent> eventHandler){
		register.addEventHandler(ActionEvent.ACTION, eventHandler);
	}
	public String getMessage(){
		String tmp = message.getText();
		message.setText("");
		return tmp;
	}
	public ListView<String> getUsers() {
		return usersList;
	}
	public ListView<String> getMessages(){
		return messagesList;
	}
	public String getUsername() {
		return username.getText();
	}
	public void setLogoutDisable(boolean state){
		logout.setDisable(state);
	}
	public void setLoginDisable(boolean state){
		login.setDisable(state);
	}
	public void setMessageDisable(boolean state){
		message.setDisable(state);
	}
}