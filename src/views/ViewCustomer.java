package views;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import fpt.com.Product;
import implementation.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Pair;
import javafx.util.StringConverter;

public class ViewCustomer extends BorderPane {
	private ListView<Product> productsList = new ListView<Product>();
	private HBox hbox = new HBox(2);
	private Button buyButton = new Button("Buy");
	private Button addButton = new Button("Add");
	private TableView<Product> orderTable = new TableView<Product>();
	private ExecutorService executorService;
	private InetAddress ia = null;
	private DatagramSocket dateClientSocket = null;
	private Label time = new Label("");


	public ViewCustomer() {
		/* Start Task 5 Part*/
		executorService = Main.mainExecutorService;
		try {
			ia = InetAddress.getByName("localhost");
		} catch (UnknownHostException e1) {
			System.out.println("Can not resolve address");
		}
		try {
			dateClientSocket = new DatagramSocket();
		} catch (SocketException e1) {
			System.out.println("TimeClient Socket could not be opened");
		}
		//create
		((ScheduledExecutorService) executorService).scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
					byte buffer[] = "DATE".getBytes();
					// Craft request packet then send it
					try{
						DatagramPacket packet = new DatagramPacket(buffer,
								buffer.length, ia, 6667);
						dateClientSocket.send(packet);

						byte answer[] = new byte[1024];
						// Paket für die Antwort vorbereiten
						packet = new DatagramPacket(answer, answer.length);
						// Wait only for 1 sec otherwise throw SocketTimeoutException
						dateClientSocket.setSoTimeout(1000);
						dateClientSocket.receive(packet);
						final DatagramPacket tempPacket = packet;
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								String comingDate = new String(tempPacket.getData(),0,tempPacket.getLength());
								time.setText(comingDate);
							}
						});
					}catch(SocketTimeoutException e2){
						//System.out.println("Time Server does not respond");
					}catch(IOException e){
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								time.setText("ERROR");
							}
						});
					}
			}
		},0,1,TimeUnit.SECONDS);
		/* End Task 5 Part */
		TableColumn<Product,String> nameCol = new TableColumn<Product,String>("Name");
		TableColumn<Product,Number> priceCol = new TableColumn<Product,Number>("Price");
		TableColumn<Product,Number> countCol = new TableColumn<Product,Number>("Buy Count");

		orderTable.getColumns().addAll(nameCol, priceCol, countCol);
		orderTable.setEditable(true);
		nameCol.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
		priceCol.setCellValueFactory(cellData -> cellData.getValue().priceProperty());
		countCol.setCellValueFactory(cellData -> cellData.getValue().quantityProperty());
		countCol.setCellFactory(TextFieldTableCell.<Product, Number>forTableColumn(
				new StringConverter(){

					@Override
					public String toString(Object object) {
						return String.valueOf(object);
					}

					@Override
					public Object fromString(String string) {
						try{
							return Integer.parseInt((String) string);
						}catch (NumberFormatException e){
							return 0;
						}
					}

				}
				));
		countCol.setOnEditCommit(
	            (CellEditEvent<Product, Number> t) -> {
	            	/*
	            	Product boughtProduct = ((Product) t.getTableView().getItems().get(t.getTablePosition().getRow()));
	            	Product listedProduct = ((ModelShop) productsList.getItems()).findProductById(boughtProduct.getId());
	            	Integer differnceQuantity = t.getNewValue().intValue() - t.getOldValue().intValue();
	            	Integer maxQuantity = listedProduct.getQuantity();
	            	if (t.getNewValue().intValue() > maxQuantity )
	            		{
	            		System.out.println("Max !");
	            			boughtProduct.setQuantity(listedProduct.getQuantity());
	            			listedProduct.setQuantity(0);
	            		}
	            	else
	            	{
	            		System.out.println("Calc !" + t.getNewValue().intValue());
	            		boughtProduct.setQuantity(t.getNewValue().intValue());
	            		listedProduct.setQuantity(listedProduct.getQuantity() - differnceQuantity);
	            	}
	            	System.out.println(t.getSource());
	            	*/
	        });
		productsList.setCellFactory(e -> {
			//System.out.println();
			ListCell<Product> cell = new ListCell<Product>() {
				@Override
				protected void updateItem(Product myObject, boolean b) {
					super.updateItem(myObject, myObject == null || b);
					if (myObject != null) {
						setText(myObject.getName() + " | " + myObject.getPrice() + " €  | " + myObject.getQuantity());
					} else {
						setText("");
					}
				}
			};
			return cell;
		});


		hbox.getChildren().addAll(addButton,buyButton,time);
		hbox.setAlignment(Pos.CENTER_RIGHT);
		setRight(orderTable);
		setLeft(productsList);
		setBottom(hbox);
	}

	public ListView<Product> getProductsList() {
		return productsList;
	}
	public TableView<Product> getOrderTable() {
		return orderTable;
	}
	public void addBuyButtonHandler(EventHandler<ActionEvent> eventHandler) {
		buyButton.addEventHandler(ActionEvent.ACTION, eventHandler);
	}
	public void addAddButtonHandler(EventHandler<ActionEvent> eventHandler){
		addButton.addEventHandler(ActionEvent.ACTION, eventHandler);
	}
	public Product getSelectedProduct() {
		//return  orderTable.getSelectionModel().getSelectedItem();
		return productsList.getSelectionModel().getSelectedItem();
	}
	public void createLoginDialog(Consumer <Pair<String, String>>  consumer){
		// Create the custom dialog.
				Dialog<Pair<String, String>> dialog = new Dialog<>();
				dialog.setTitle("Login Dialog");
				dialog.setHeaderText("enter username & password , tip : admin , admin ! ");

				// Set the button types.
				ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
				dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

				// Create the username and password labels and fields.
				GridPane grid = new GridPane();
				grid.setHgap(10);
				grid.setVgap(10);
				grid.setPadding(new Insets(20, 150, 10, 10));

				TextField username = new TextField();
				username.setPromptText("Username");
				PasswordField password = new PasswordField();
				password.setPromptText("Password");

				grid.add(new Label("Username:"), 0, 0);
				grid.add(username, 1, 0);
				grid.add(new Label("Password:"), 0, 1);
				grid.add(password, 1, 1);

				// Enable/Disable login button depending on whether a username was entered.
				Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
				loginButton.setDisable(true);

				// Do some validation (using the Java 8 lambda syntax).
				username.textProperty().addListener((observable, oldValue, newValue) -> {
				    loginButton.setDisable(newValue.trim().isEmpty());
				});

				dialog.getDialogPane().setContent(grid);

				// Request focus on the username field by default.
				Platform.runLater(() -> username.requestFocus());

				// Convert the result to a username-password-pair when the login button is clicked.
				dialog.setResultConverter(dialogButton -> {
				    if (dialogButton == loginButtonType) {
				        return new Pair<>(username.getText(), password.getText());
				    }
				    return null;
				});

				Optional<Pair<String, String>> result = dialog.showAndWait();
				result.ifPresent(consumer);
	}
}