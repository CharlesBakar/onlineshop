package views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import strategies.BinaryStrategy;
import strategies.JDBCStrategy;
import strategies.OpenJPAStrategy;
import strategies.XMLStrategy;
import strategies.XStreamStrategy;
import fpt.com.Product;
import fpt.com.SerializableStrategy;

public class ViewShop extends BorderPane {

	Product product;

	private Label nameLabel = new Label("Name:");
	private Label priceLabel = new Label("Price:");
	private Label countLabel = new Label("Count:");
	private Button addButton = new Button("Add");
	private Button deleteButton = new Button("Delete");
	private Button ladenButton = new Button("Laden");
	private Button speichernButton = new Button("Speichern");
	private HBox hbox = new HBox(2);
	private HBox speichernBox = new HBox(2);
	private VBox vbox = new VBox(2);
	private TextField nameText = new TextField();
	private TextField priceText = new TextField();
	private TextField countText = new TextField();
	private ListView<Product> products = new ListView<Product>();
	private ChoiceBox<SerializableStrategy> serializingMethod = new ChoiceBox<SerializableStrategy>();

	public ViewShop() {

		hbox.getChildren().addAll(addButton, deleteButton);
		vbox.getChildren().addAll(nameLabel, nameText, priceLabel, priceText, countLabel, countText, hbox);
		speichernBox.getChildren().addAll(serializingMethod,ladenButton,speichernButton);
		speichernBox.setPadding(new Insets(0, 0, 5, 0));
		// Serialisierung Stratiegies Hinzufügen
		serializingMethod.getItems().addAll(new BinaryStrategy(), new XMLStrategy(), new XStreamStrategy(),new JDBCStrategy()
				,new OpenJPAStrategy());
		setCenter(products);
		setRight(vbox);
		setTop(speichernBox);

		products.setCellFactory(e -> {
			ListCell<Product> cell = new ListCell<Product>() {
				@Override
				protected void updateItem(Product myObject, boolean b) {
					super.updateItem(myObject, myObject == null || b);
					if (myObject != null) {
						setText(myObject.getName() + " | " + myObject.getPrice() + " €  | " + myObject.getQuantity());
					} else {
						setText("");
					}
				}
			};
			return cell;
		});

	}

	public void addAddButtonHandler(EventHandler<ActionEvent> eventHandler) {

		addButton.addEventHandler(ActionEvent.ACTION, eventHandler);

	}

	public void addDeleteButtonHandler(EventHandler<ActionEvent> eventHandler1) {
		deleteButton.addEventHandler(ActionEvent.ACTION, eventHandler1);

	}

	public void addLadenButtonHandler(EventHandler<ActionEvent> eventHandler) {

		ladenButton.addEventHandler(ActionEvent.ACTION, eventHandler);

	}
	public void addSpeichernButtonHandler(EventHandler<ActionEvent> eventHandler) {

		speichernButton.addEventHandler(ActionEvent.ACTION, eventHandler);

	}
	//return the selected Product ID from viewlist
	public int getSelectedProduct() {
		return products.getSelectionModel().getSelectedIndex();
	}

	/**
	 * @return
	 */
	public Product getInput() {
		if(nameText.getText().equals("") || priceText.getText().equals("") || countText.getText().equals(""))
			return null;
		try {
			return new implementation.Product(nameText.getText(), Double.parseDouble(priceText.getText()),
					Integer.parseInt(countText.getText()));
		} catch (NumberFormatException e) {
			return null;
		}

	}

	public ListView<Product> getProducts() {
		return products;
	}
	public SerializableStrategy getSelectedStrategy(){
		return (SerializableStrategy) serializingMethod.getValue();
	}

}