package warehouse;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ExecutorService;

import implementation.Order;

public class Connection {
	private IncomingThread incomingThread;
	private OutgoingThread outgoingThread;
	private Socket socket;
	private ExecutorService executorService;
	private OrdersContainer ordersContainer;
	private boolean status = false;
	private String hostAddress;

	public Connection(Socket socket,ExecutorService executorService,OrdersContainer ordersContainer){
		this.socket = socket;
		this.executorService = executorService;
		this.ordersContainer = ordersContainer;
		try {
			incomingThread = new IncomingThread(socket.getInputStream(),this);
			outgoingThread = new OutgoingThread(socket.getOutputStream(),this);
			executorService.execute(incomingThread);
			executorService.execute(outgoingThread);
			this.status = true;
			hostAddress = new String(socket.getInetAddress().getHostAddress());
			System.out.println("Connection Established " + hostAddress);
		} catch (IOException e) {
			System.out.println("Connection could not be established");
		}
	}
	public void accessDenied(){
		System.out.println("Access denied for " + hostAddress);
		outgoingThread.sendText("denied:");
	}
	public void addOrder(Order order){
		ordersContainer.addOrder(order);
		outgoingThread.sendOrder(order);
	}
	public synchronized void setStatus(boolean status){
		this.status = status;
	}
	public synchronized boolean getStatus(){
		return status;
	}
	public void close(){
		if(getStatus()){
			System.out.println("Terminating connection " + hostAddress);
			setStatus(false);
			incomingThread.stop();
			outgoingThread.stop();
		}
	}
}
