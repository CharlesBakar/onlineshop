package warehouse;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.Scanner;

import implementation.Order;

public class IncomingThread implements Runnable {
	private Connection conn;
	private InputStream inputStream;
	private DataInputStream inputDataStream;
	private ObjectInputStream inputObjectStream;

	public IncomingThread(InputStream input, Connection conn) {
		this.conn = conn;
		inputStream = input;
		inputDataStream = new DataInputStream(input);
		System.out.println("Incoming Thread created");
	}

	@Override
	public void run() {
		//System.out.println("Incoming thread " + Thread.currentThread().getName());
		while (conn.getStatus()) {
			try {
				String command = inputDataStream.readUTF();
				Scanner sc = new Scanner(command).useDelimiter(":");

				// Erstes Kommando filtern
				String keyword = sc.next();

				if (keyword.equals("order")) {
					try {
						inputObjectStream = new ObjectInputStream(inputStream);
						Order order;
						try {
							order = (Order) inputObjectStream.readObject();
							if (sc.next().equals("admin") && sc.next().equals("admin"))
								conn.addOrder(order);
							else
								conn.accessDenied();
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (keyword.equals("tobefilledlater")) {

				}
			} catch (IOException e) {
				conn.close();
			}
		}
	}

	public void stop() {
		try {
			inputObjectStream.close();
			inputDataStream.close();
			inputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}