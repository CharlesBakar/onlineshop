package warehouse;

import java.util.Iterator;
import java.util.Vector;

import implementation.Order;
import implementation.Product;

public class OrdersContainer {
	private Vector<Order> ordersList = new Vector<Order>();
	public OrdersContainer(){

	}
	public synchronized void addOrder(Order order){
		ordersList.add(order);
		System.out.println("Order eingegangen:");
		for(fpt.com.Product p : order){
			System.out.println(p.getName()+"\t"+p.getQuantity()+"\t"+p.getPrice()+" EUR");
		}
		System.out.println("");
		displayList();
	}
	public synchronized void displayList(){
		int totalQuantity = 0;
		double totalPrice = 0;
		System.out.println("Orders gesamt");
		System.out.println("=============================================");
		for(Order o : ordersList){
			for(fpt.com.Product p : o){
				totalPrice+=p.getPrice()*p.getQuantity();
				totalQuantity+=p.getQuantity();
				System.out.println(p.getName()+"\t"+p.getQuantity()+"\t"+p.getPrice()+" EUR");
			}
		}
		System.out.println("=============================================");
		System.out.println("Gesamtanzahl: " + totalQuantity);
		System.out.println("Gesamtwert: " + totalPrice);
	}
}
