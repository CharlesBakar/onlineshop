package warehouse;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import implementation.Order;
import implementation.Product;

public class OutgoingThread implements Runnable{
	private OutputStream outputStream;
	private DataOutputStream outputDataStream;
	private Connection conn;

	public OutgoingThread(OutputStream out,Connection conn) {
		outputStream = out;
		outputDataStream = new DataOutputStream(out);
		this.conn = conn;
	}
	@Override
	public void run() {
		//System.out.println("Outgoing "+ Thread.currentThread().getName());
		while(conn.getStatus()){
			/*try {
				outputDataStream.writeUTF("ECHO");
			} catch (IOException e1) {
				System.out.println("Broken Pipe probably connection closed, terminate connection");
				conn.close();
			}*/
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void stop() {
		try {
			outputDataStream.close();
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void sendOrder(Order order) {
		try {
			outputDataStream.writeUTF("clone:");
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(order);
			outputStream.flush();
		} catch (IOException e) {
			System.out.println("Could not send the order");
			conn.close();
		}
	}
	public void sendText(String cmd) {
		try {
			outputDataStream.writeUTF(cmd);
			outputStream.flush();
		} catch (IOException e) {
			System.out.println("Could not send the text");
			conn.close();
		}
	}
}
