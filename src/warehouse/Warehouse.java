package warehouse;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import implementation.Order;

public class Warehouse {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newCachedThreadPool();
		OrdersContainer ordersContainer = new OrdersContainer();
		ArrayList<Connection> connections = new ArrayList<Connection>();

		// lunch date server
		try {
			final DatagramSocket dateServerSocket = new DatagramSocket(6667);
			DatagramPacket incomingPacket = new DatagramPacket(new byte[4], 4);
			executorService.execute(new Runnable() {

				@Override
				public void run() {
					try {
						System.out.println("Date UDP Server has started");
						while (true) {
							dateServerSocket.receive(incomingPacket);
							if (new String(incomingPacket.getData()).equals("DATE")) {
								InetAddress address = incomingPacket.getAddress();
								int port = incomingPacket.getPort();
								int len = incomingPacket.getLength();
								Date dt = new Date();
								SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
								byte[] myDate = sdf.format(new Date()).toString().getBytes();
								DatagramPacket responsePacket = new DatagramPacket(myDate, myDate.length, address,
										port);

								try {
									dateServerSocket.send(responsePacket);
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
		} catch (SocketException e) {
			System.out.println("TimeSocketServer could not be established");
		}

		//Lunch TCP Server
		try {
			ServerSocket ordersServer = new ServerSocket(6666);
			executorService.execute(new Runnable() {

				@Override
				public void run() {
					System.out.println("I am the seeker behind clients "+ Thread.currentThread().getName());
					while(true){
						try {
							Socket socket = ordersServer.accept();
							connections.add(new Connection(socket,executorService,ordersContainer));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
		} catch (IOException e) {
			System.out.println("OrdersServer could not be established");
		}

		/*boolean shutdown = false;
		while(!shutdown){
			Console console = System.console();
			String cmd = console.readLine();
			switch(cmd){
				case "shutdown":
					shutdown = true;
					break;
			}
		}
		executorService.shutdown();*/
	}

}
